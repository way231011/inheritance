import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ModelGitar {

        public String model = "Ibanez";
        public String senar = "7 Senar";

        public void HargaGitar() {
            int a = 5000000;
            DecimalFormat kursIdn = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
            formatRp.setCurrencySymbol("Rp. ");
            formatRp.setMonetaryDecimalSeparator(',');
            formatRp.setGroupingSeparator('.');

            kursIdn.setDecimalFormatSymbols(formatRp);
            System.out.println(kursIdn.format(a));
        }
}
